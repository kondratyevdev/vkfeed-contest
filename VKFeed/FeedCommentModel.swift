//
//  FeedCommentModel.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/20/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

class FeedCommentModel {
   
    var date = 0
    var user: VKUser!
    var text: String!
    
    init(feedComment: VKFeedPhotoComment) {
        self.user = UsersManager.get(feedComment.from_id.integerValue)
        
        self.text = feedComment.text
        self.date = feedComment.date.integerValue
    }
    
    func attributedStringForFeed() -> NSAttributedString {
        let mutableAttributedString = NSMutableAttributedString()
        
       
        mutableAttributedString.appendAttributedString(NSAttributedString(string: "[id\(self.user.id.integerValue)|\(self.user.fullname)]", attributes: [
            NSFontAttributeName: UIFont(name: "HelveticaNeue-Medium", size: 15)!,
        ]))
        
        mutableAttributedString.appendAttributedString(NSAttributedString(string: " " + self.text, attributes: [
            NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 15)!,
            NSForegroundColorAttributeName: UIColorFromRGB(0x666666),
        ]))
        
        let mutableParagraphStyle = NSMutableParagraphStyle()
        mutableParagraphStyle.maximumLineHeight = 19
        mutableParagraphStyle.minimumLineHeight = 19
        
        mutableAttributedString.addAttribute(NSParagraphStyleAttributeName, value: mutableParagraphStyle, range: NSMakeRange(0, mutableAttributedString.length))
        mutableAttributedString.addAttribute(NSKernAttributeName, value: 0.22, range: NSMakeRange(0, mutableAttributedString.length))

        
        return mutableAttributedString
    }
}
