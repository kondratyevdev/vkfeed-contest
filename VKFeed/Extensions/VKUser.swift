//
//  VKUser.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/21/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

extension VKUser {
    
    var fullname: String {
        return (self.first_name + " " + self.last_name).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
    }
    
}
