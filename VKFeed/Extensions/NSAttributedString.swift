//
//  NSAttributedString.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/21/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
   
    enum DetectAttributedType {
        case Tags, Links, VKLinks
    }
    
    func _detectAndAddLinkAttributes(regex: NSRegularExpression, type: DetectAttributedType) {
        let matches = regex.matchesInString(self.string, options: NSMatchingOptions.allZeros, range: NSMakeRange(0, countElements(self.string)))
        
        var checkingResultList: Array<NSTextCheckingResult> = Array()
        
        for match in matches {
            checkingResultList.append(match as NSTextCheckingResult)
        }
        
        checkingResultList = checkingResultList.reverse()
        for checkingResult in checkingResultList {
            if(type == .VKLinks) {
                let text = (self.string as NSString).substringWithRange(checkingResult.rangeAtIndex(2))
                let id = (self.string as NSString).substringWithRange(checkingResult.rangeAtIndex(1))
                var url = NSURL(string: "vklink://" + id)!
                
                self.addAttribute(NSLinkAttributeName, value: url, range: checkingResult.range)
                self.replaceCharactersInRange(checkingResult.range, withString: text)

            } else {
                let text = (self.string as NSString).substringWithRange(checkingResult.range)
                
                var url =  NSURL(string: type == .Links ? text : "tag://" + text.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
                if(url != nil) {
                    self.addAttribute(NSLinkAttributeName, value: url!, range: checkingResult.range)
                }
            }
        }
    }
    
    func detectLinksAndAddLinkAttributes() {
        let detector = NSDataDetector(types: NSTextCheckingType.Link.rawValue, error: nil)
        self._detectAndAddLinkAttributes(detector!, type: .Links)
    }
    
    func detectTagsAndAddLinkAttributes() {
        let regex = NSRegularExpression(pattern: "#(\\w+)", options: NSRegularExpressionOptions.allZeros, error: nil)
        self._detectAndAddLinkAttributes(regex!, type: .Tags)
    }
    
    func detectVKLinksAndAddAttributes() {
        let regex = NSRegularExpression(pattern: "\\[id(\\d+)\\|([^\\]]+)\\]", options: NSRegularExpressionOptions.allZeros, error: nil)
        self._detectAndAddLinkAttributes(regex!, type: .VKLinks)

    }
}
