//
//  FeedLikesView.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/18/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

let FeedLikesCount = Int((PHOTO_WIDTH - 60) / 44)

class FeedLikesView: UIView {

    var moreLikesButton: UIButton!
    var userLikeViews: Array<UserLikeView> = Array()
    var likes: Array<VKUser>! {
        didSet {
            self.likesDidChanged()
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        for(var i = 0; i < FeedLikesCount; i++) {
            let userLikeView = UserLikeView()
            userLikeView.frame.offset(dx: CGFloat(44 * i), dy: 0)
            self.addSubview(userLikeView)
            self.userLikeViews.append(userLikeView)
        }
        
        self.moreLikesButton = UIButton()
        self.moreLikesButton.setBackgroundImage(UIImage(named: "ListLikeMore"), forState: UIControlState.Normal)
        self.moreLikesButton.setBackgroundImage(UIImage(named: "ListLikeMore_pressed"), forState: UIControlState.Highlighted)
        self.moreLikesButton.sizeToFit()
        self.moreLikesButton.frame.offset(dx: CGFloat(44 * FeedLikesCount), dy: 0)
        self.addSubview(self.moreLikesButton)
    }
    
    func cancelAll() {
        for userLikeView in self.userLikeViews {
            userLikeView.cancel()
        }
    }
    
    func likesDidChanged() {
        for (index, userLikeView) in enumerate(self.userLikeViews) {
            if(index < self.likes.count) {
                userLikeView.hidden = false
                userLikeView.user = self.likes[index]
            } else {
                userLikeView.hidden = true
                userLikeView.cancel()
            }
        }
        
        self.moreLikesButton.hidden = self.likes.count != FeedLikesCount
    }
    
}
