//
//  FeedPhotoImageView.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/14/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

class FeedPhotoImageView : UIImageView {
    
    var photo: FeedPhotoModel! {
        didSet {
            if(self.photo.src != oldValue?.src || !CGRectEqualToRect(self.photo.frame, oldValue.frame)) {
                self.photoDidChanged()
            }
        }
    }
    
    var loadFromDistOperation: NSOperation!
    var downloadOperation: SDWebImageOperation!
    var placeholderImage: UIImage! = nil
    var startTime: Double = 0

    override init() {
        super.init()
        
        self.placeholderImage = coloredImage(CGSizeMake(1, 1), UIColorFromRGB(0xdcdfe3))
        self.image = self.placeholderImage
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    var photosCountView: UIView!
    var photosCountLabel: UILabel!
    func showPhotosCount(count: Int) {
        
        if(self.photosCountView == nil) {
            self.photosCountView = UIView(frame: self.bounds)
            self.photosCountView.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
            self.photosCountView.backgroundColor = UIColor(white: 0, alpha: 0.8)
            self.addSubview(self.photosCountView)
            
            self.photosCountLabel = UILabel(frame: CGRectMake(0, 0, self.bounds.size.width - 20, self.bounds.size.height - 20))
            self.photosCountLabel.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
            self.photosCountLabel.textColor = UIColor.whiteColor()
            self.photosCountLabel.font = UIFont.systemFontOfSize(45)
            self.photosCountView.addSubview(self.photosCountLabel)
        }
        
        self.photosCountView.hidden = false
        self.photosCountLabel.text = "\(count)"
        self.photosCountLabel.sizeToFit()
        self.photosCountLabel.center = CGPointMake(self.photosCountView.frame.size.width / 2, self.photosCountView.frame.size.height / 2)
    }
    
    func hidePhotosCount() {
        self.photosCountView?.hidden = true
    }
    
    func cancel() {
        self.loadFromDistOperation?.cancel()
        self.downloadOperation?.cancel()
        
        self.loadFromDistOperation = nil
        self.downloadOperation = nil
    }
    
    func photoDidChanged() {
        self.cancel()
        self.frame = self.photo.frame
        
        self.startTime = CACurrentMediaTime()
        
        let cacheUrl = self.photo.src
        let cacheKey = "\(self.photo.src)_\(self.photo.frame.size.width)_\(self.photo.frame.size.height)"
        
        let cachedImage = SDImageCache.sharedImageCache().imageFromMemoryCacheForKey(cacheKey)
        if(cachedImage != nil) {
            self.setImage(cachedImage, animation: false)
            return
        }
        
        self.setImage(self.placeholderImage, animation: false)
        
        self.loadFromDistOperation = SDImageCache.sharedImageCache().queryDiskCacheForKey(cacheKey, done: { (cachedImage, type) -> Void in
            if(cachedImage != nil) {
                if(cacheUrl == self.photo.src) {
                    self.setImage(cachedImage, animation: CACurrentMediaTime() - self.startTime > 0.08)
                }
                return
            }
        
            self.downloadOperation = SDWebImageDownloader.sharedDownloader().downloadImageWithURL(NSURL(string: cacheUrl), options: SDWebImageDownloaderOptions.ContinueInBackground, progress: nil) { (image, data, error, finished) -> Void in
                
                if(finished && image != nil) {
                    var newImage = UIImage.decodedImageWithImage(cropImage(image, self.frame.size))
                    
                    NAQueueMain.dispatchOnQueue({ () -> Void in
                        SDImageCache.sharedImageCache().storeImage(newImage, forKey: cacheKey, toDisk: true)
                        if(cacheUrl == self.photo.src) {
                            self.setImage(newImage, animation: true)
                        }
                    })
                }
            }
        })
    }
    
    private func setImage(image: UIImage, animation: Bool) {
        self.image = image
        if(animation) {
            var animation: CATransition = CATransition()
            animation.duration = 0.2
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            animation.type = kCATransitionFade
            self.layer.addAnimation(animation, forKey: "imageFade")
        } else {
            self.layer.removeAnimationForKey("imageFade")
        }
    }
}
