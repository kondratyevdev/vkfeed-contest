//
//  UserAvatarImageView.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/14/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit
import QuartzCore

let UserAvatarCache = SDImageCache()

class UserAvatarImageView: UIImageView {

    var user: VKUser! {
        didSet {
            if(self.user?.id != oldValue?.id) {
                self.userChanged()
            }
        }
    }
    
    var syncWithDisk = true
    
    var loadFromDistOperation: NSOperation!
    var downloadOperation: SDWebImageOperation!
    var placeholderImage: UIImage!
    var startTime: Double = 0
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.placeholderImage = roundedImage(coloredImage(frame.size, UIColorFromRGB(0xdcdfe3)), frame.size)
    }
    
    
    func cancel() {
        self.loadFromDistOperation?.cancel()
        self.downloadOperation?.cancel()
        
        self.loadFromDistOperation = nil
        self.downloadOperation = nil
    }
    
//    private func userChanged() {
//        self.cancel()
//        
//        self.startTime = CACurrentMediaTime()
//        
//        let cacheKey = self.user.photo_100 + "map"
//        let cachePath = UserAvatarCache.defaultCachePathForKey(cacheKey)
//        let cachedImage = UserAvatarCache.imageFromMemoryCacheForKey(cacheKey)
//
//        if(cachedImage != nil) {
//            self.setImage(cachedImage, animation: false)
//            NSLog("endAvatar cache image \(CACurrentMediaTime() - self.startTime)")
//            return
//        }
//        
//        let cachedMapImage = UIImage(mapFromPath: cachePath)
//
//        if(cachedMapImage != nil) {
//            self.setImage(cachedMapImage, animation: false)
//            UserAvatarCache.storeImage(cachedMapImage, forKey: cacheKey, toDisk: false)
//            NSLog("endAvatar map image \(CACurrentMediaTime() - self.startTime)")
//            return
//        }
//        
//        self.setImage(self.placeholderImage, animation: false)
//        self.downloadOperation = SDWebImageDownloader.sharedDownloader().downloadImageWithURL(NSURL(string: self.user.photo_100), options: SDWebImageDownloaderOptions.ContinueInBackground, progress: nil) { (image, data, error, finished) -> Void in
//            
//            if(finished && image != nil) {
//                let newImage = roundedImage(image, image.size.width).decompressAndMapToPath(cachePath)
//                
//                NAQueueMain.dispatchOnQueue({ () -> Void in
//                    UserAvatarCache.storeImage(newImage, forKey: cacheKey, toDisk: false)
//                    if(cacheKey == self.user.photo_100 + "map") {
//                        self.setImage(newImage, animation: CACurrentMediaTime() - self.startTime > 0.08)
//                        NSLog("endAvatar loading \(CACurrentMediaTime() - self.startTime)")
//                    }
//                })
//            }
//        }
//    }

    private func userChanged() {
        self.cancel()
        
        self.startTime = CACurrentMediaTime()

        let cacheKey = self.user.photo_100
        let cachedImage = self.syncWithDisk ? UserAvatarCache.imageFromDiskCacheForKey(cacheKey) : UserAvatarCache.imageFromMemoryCacheForKey(cacheKey)
        
//        UserAvatarCache.removeImageForKey(cacheKey, fromDisk: true)
        
        if(cachedImage != nil) {
            self.setImage(cachedImage, animation: false)
            return
        }
        
        self.setImage(self.placeholderImage, animation: false)
        
        let completedBlock: SDWebImageQueryCompletedBlock = { (cachedImage, type) -> Void in
            if(cachedImage != nil) {
                if(cacheKey == self.user.photo_100) {
                    self.setImage(cachedImage, animation: CACurrentMediaTime() - self.startTime > 0.08)
                }
                return
            }
            
            self.downloadOperation = SDWebImageDownloader.sharedDownloader().downloadImageWithURL(NSURL(string: cacheKey), options: SDWebImageDownloaderOptions.ContinueInBackground, progress: nil) { (image, data, error, finished) -> Void in
                
                if(finished && image != nil) {
                    var newImage = roundedImage(image, CGSizeMake(45, 45))
                    newImage = UIImage.decodedImageWithImage(newImage)
                    
                    NAQueueMain.dispatchOnQueue({ () -> Void in
                        UserAvatarCache.storeImage(newImage, forKey: cacheKey, toDisk: true)
                        if(cacheKey == self.user.photo_100) {
                            self.setImage(newImage, animation: CACurrentMediaTime() - self.startTime > 0.08)
                        }
                    })
                }
            }
        }
        
        if(self.syncWithDisk) {
            completedBlock(nil, SDImageCacheType.None)
        } else {
            self.loadFromDistOperation = UserAvatarCache.queryDiskCacheForKey(cacheKey, done: completedBlock)
        }
    }
    
    private func setImage(image: UIImage, animation: Bool) {
        self.image = image
        if(animation) {
            var animation:CATransition = CATransition()
            animation.duration = 0.2
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            animation.type = kCATransitionFade
            self.layer.addAnimation(animation, forKey: "imageFade")
        } else {
            self.layer.removeAnimationForKey("imageFade")
        }
    }
}
