//
//  UserLikeView.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/18/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

class UserLikeView : UIImageView {
    
    var user: VKUser! {
        didSet {
            if(self.user?.id.integerValue != oldValue?.id.integerValue) {
                self.userDidChanged()
            }
        }
    }
    
    var userAvatarImageView: UserAvatarImageView!
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init() {
        super.init(frame: CGRectMake(0, 0, 39, 39))
        
        self.userAvatarImageView = UserAvatarImageView(frame: CGRectMake(0, 0, 34, 34))
        self.userAvatarImageView.syncWithDisk = false
        self.addSubview(self.userAvatarImageView)
        
        let likeMarkImageView = UIImageView(image: UIImage(named: "ListLikeMark"))
        likeMarkImageView.frame.offset(dx: frame.size.width - likeMarkImageView.frame.size.width - OnePixelInset, dy: frame.size.height - likeMarkImageView.frame.size.height - OnePixelInset)
        self.addSubview(likeMarkImageView)
    }
    
    func cancel() {
        self.userAvatarImageView.cancel()
    }
    
    func userDidChanged() {
        self.userAvatarImageView.user = user
    }
    
}
