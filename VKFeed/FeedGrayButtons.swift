//
//  GrayButtons.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/14/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

class FeedGrayButton : UIButton {
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init() {
        super.init()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.titleLabel!.font = UIFont(name: "HelveticaNeue-Medium", size: 14)
        self.setTitleColor(UIColorFromRGB(0x949799), forState: UIControlState.Normal)
        self.setBackgroundImage(UIImage(named: "ButtonBg")?.resizableImageWithCapInsets(UIEdgeInsetsMake(17, 3, 17, 3)), forState: UIControlState.Normal)
        self.setBackgroundImage(UIImage(named: "ButtonBg_pressed")?.resizableImageWithCapInsets(UIEdgeInsetsMake(17, 3, 17, 3)), forState: UIControlState.Highlighted)
    }
}

class FeedStandartGrayButton : FeedGrayButton {
    override func sizeToFit() {
        super.sizeToFit()
        self.frame.size = CGSizeMake(self.frame.size.width + 20 + OnePixelInset, 34);
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.titleLabel!.frame.offset(dx: OnePixelInset, dy: 0)
    }
}

class FeedLikeGrayButton : FeedGrayButton {
    
    var likesCount: Int = -1 {
        didSet {
            if(self.likesCount != oldValue) {
                self.setTitle(self.likesCount != 0 ? "\(self.likesCount)" : nil, forState: UIControlState.Normal)
                self.sizeToFit()
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setImage(UIImage(named: "ButtonLikeIcon"), forState: UIControlState.Normal)
    }
    
    override func sizeToFit() {
        super.sizeToFit()
        
        if(self.likesCount == 0) {
            self.frame.size = CGSizeMake(44, 34);
        } else {
            self.frame.size = CGSizeMake(41 + OnePixelInset + self.titleLabel!.frame.size.width, 34);
        }
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if(self.likesCount == 0) {
            self.imageView!.frame.offset(dx: 0, dy: -OnePixelInset)
        } else {
            self.imageView!.frame.offset(dx: -2, dy: -OnePixelInset)
            self.titleLabel!.frame.offset(dx: 3, dy: 0)
        }
    }
}
