//
//  FeedViewController.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/12/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

class FeedViewController: UITableViewController, FeedPostsManagerProtocol {
    
    var manager: FeedPostsManager!
    
    override func loadView() {
        super.loadView()
        
        self.manager = FeedPostsManager()
        self.manager.delegate = self
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        self.tableView.rowHeight = 100
        
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.tintColor = UIColor.grayColor()
        self.refreshControl!.beginRefreshing()
        self.refreshControl!.addTarget(self, action: "refreshControlAction", forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func refreshControlAction() {
        self.manager.refreshFeed()
    }
    
    func feedPostsManager(manager: FeedPostsManager?, addItemsInRange: NSRange) {
        
        if(addItemsInRange.location == 0) {
            self.refreshControl!.endRefreshing()
            self.tableView.reloadData()
        } else {
            self.tableView.insertSections(NSIndexSet(indexesInRange: addItemsInRange), withRowAnimation: UITableViewRowAnimation.None)
        }
    }
    
    func feedPostsManager(manager: FeedPostsManager?, error: NSError) {
        if(self.manager.count == 0) {
            self.refreshControl!.endRefreshing()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    var oldViewFrame: CGRect = CGRectZero
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.oldViewFrame = self.view.frame
        
        self.view.frame = CGRectMake(self.oldViewFrame.origin.x, self.oldViewFrame.origin.y - self.oldViewFrame.size.height, self.oldViewFrame.size.width, self.oldViewFrame.size.height * 3)
        self.tableView.contentInset = UIEdgeInsetsMake(self.tableView.contentInset.top + self.oldViewFrame.size.height, 0, self.oldViewFrame.size.height, 0)
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(self.tableView.scrollIndicatorInsets.top + self.oldViewFrame.size.height, self.tableView.scrollIndicatorInsets.left, self.oldViewFrame.size.height, self.tableView.scrollIndicatorInsets.right)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.manager.count
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 65
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.manager.objectAtIndex(indexPath.section).rowHeight
    }
    
    private var headerCells: Array<FeedHeaderCellView> = Array()
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var cell: FeedHeaderCellView! = nil;
        
        for view in self.headerCells {
            if(view.superview == nil) {
                cell = view
                break
            }
        }
        
        if(cell == nil) {
            cell = FeedHeaderCellView(frame: CGRectMake(0, 0, self.view.frame.size.width, 65))
            self.headerCells.append(cell)
        }
        
        cell.feedPost = self.manager.objectAtIndex(section)
        return cell
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(indexPath.section + 5 > self.manager.count) {
            self.manager.loadMoreItems()
        }
        
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell") as? FeedCellView
        if(cell == nil) {
            cell = FeedCellView(frame: self.view.frame)
        }
        
        cell!.feedPost = self.manager.objectAtIndex(indexPath.section)
        return cell!
    }

}
