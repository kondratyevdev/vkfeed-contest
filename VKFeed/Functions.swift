//
//  Functions.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/12/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

func UIColorFromRGB(rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

var OnePixelInset:CGFloat { return 1.0 / UIScreen.mainScreen().scale }

func CGRectNormalizeForScale(rect: CGRect) -> CGRect {
    let scale = UIScreen.mainScreen().scale
    return CGRectMake(ceil(rect.origin.x * scale) / scale, ceil(rect.origin.y * scale) / scale, ceil(rect.size.width * scale) / scale, ceil(rect.size.height * scale) / scale)
}

func normalizePixexForDeviceScale(pixel: CGFloat) -> CGFloat {
    let scale = UIScreen.mainScreen().scale
    
    return round(pixel * scale) / scale
}

func extendImage(image: UIImage, insets: UIEdgeInsets) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(image.size.width + insets.left + insets.right, image.size.height + insets.top + insets.bottom), false, UIScreen.mainScreen().scale)
    image.drawAtPoint(CGPointMake(insets.left, insets.top))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return newImage
}

func NALocalizedString(key: String) -> String? {
    return NSLocalizedString(key, tableName: nil, bundle: NSBundle.mainBundle(), value: key, comment: key)
}

func coloredImage(size: CGSize, color: UIColor) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.mainScreen().scale)
    let rect = CGRectMake(0, 0, size.width, size.height)
    color.setFill()
    UIBezierPath(rect: rect).fill()
    let newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage
}

func roundedImage(image: UIImage!, size: CGSize) -> UIImage {
    
    let scaledImageSize = CGSizeMake(image.size.width * image.scale, image.size.height * image.scale)
    let scaledSize = CGSizeMake(size.width * UIScreen.mainScreen().scale, size.width * UIScreen.mainScreen().scale)
    
    var resultSize = size
    var resultScale = UIScreen.mainScreen().scale
    if(scaledSize.width > scaledImageSize.width) {
        resultScale = image.scale
        resultSize = image.size
    }
    
    UIGraphicsBeginImageContextWithOptions(resultSize, false, resultScale)
    let rect = CGRectMake(0, 0, resultSize.width, resultSize.height)
    UIBezierPath(roundedRect: rect, cornerRadius: size.width).addClip()
    image.drawInRect(rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage
}

func cropImage(scImage: UIImage, targetSize: CGSize) -> UIImage! {
    
    let sourceImage = UIImage(CGImage: scImage.CGImage, scale: UIScreen.mainScreen().scale, orientation: scImage.imageOrientation)!
    
    let scale = sourceImage.scale
    let width = sourceImage.size.width
    let height = sourceImage.size.height
    
    let targetScale = UIScreen.mainScreen().scale
    let targetWidth = targetSize.width
    let targetHeight = targetSize.height
    
    var resultScale = targetScale
    var resultWidth = targetWidth
    var resultHeight = targetHeight
    
    var widthFactor = targetWidth / width;
    var heightFactor = targetHeight / height;
    var scaleFactor: CGFloat = 0.0;

    var scaledWidth = targetWidth
    var scaledHeight = targetHeight
    
    var thumbnailPoint = CGPointZero
    
    if (widthFactor > heightFactor) {
        scaleFactor = widthFactor // scale to fit height
    } else {
        scaleFactor = heightFactor // scale to fit width
    }
    
    scaledWidth  = width
    scaledHeight = height
    
    if(scaleFactor < 1) {
        scaledWidth  *= scaleFactor
        scaledHeight *= scaleFactor
    } else {
        resultWidth = targetWidth / scaleFactor
        resultHeight = targetHeight / scaleFactor
        resultScale = scale
    }
    
    if (widthFactor > heightFactor) {
        thumbnailPoint.y = (resultHeight - scaledHeight) * 0.5;
    } else {
        if (widthFactor < heightFactor) {
            thumbnailPoint.x = (resultWidth - scaledWidth) * 0.5;
        }
    }
    
    let resultSize = CGSizeMake(floor(resultWidth), floor(resultHeight))
    UIGraphicsBeginImageContextWithOptions(resultSize, true, resultScale)
    let thumbnailRect = CGRectMake(thumbnailPoint.x, thumbnailPoint.y, scaledWidth, scaledHeight)
    sourceImage.drawInRect(thumbnailRect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

func relativeDateStringFromTimestamp(timestamp: NSTimeInterval) -> String {
    let now = NSDate()
    let date = NSDate(timeIntervalSince1970: timestamp)
    let deltaSeconds = fabs(date.timeIntervalSinceDate(now))
    
    if(deltaSeconds < 60) {
        return NSString(format: "%d с", Int(deltaSeconds))
    }
    
    let deltaMinutes = deltaSeconds / 60.0
    if(deltaMinutes < 60) {
        return NSString(format: "%d мин", Int(deltaMinutes))
    }
    
    let deltaHours = deltaMinutes / 60.0
    if(deltaHours < 24) {
        return NSString(format: "%d ч", Int(deltaHours))
    }
    
    let deltaDays = deltaHours / 24.0
    return NSString(format: "%d д", Int(deltaDays))
}

func showAlertView(title: String!, message: String!) {
    UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "Ok").show()
}

//#define TTTLocalizedPluralString(count, singular, comment) \
//[NSString stringWithFormat:[[NSBundle mainBundle] localizedStringForKey:TTTLocalizedPluralStringKeyForCountAndSingularNoun(count, singular) value:@"" table:nil], count]


func TTTLocalizedPluralString(count: Int, singular: String, comment: String! = nil) -> String {
    let format = NSBundle.mainBundle().localizedStringForKey(TTTLocalizedPluralStringKeyForCountAndSingularNoun(UInt(count), singular), value: "", table: nil)
    return NSString(format: format, count)
}