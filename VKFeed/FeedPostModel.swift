//
//  FeedPostModel.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/13/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

class FeedPostModel {
    
    var uniqueIndex: String!
    
    var vkFeedPost: VKFeedPost
    
    var user: VKUser?
    var userFullname: String?
    
    var statusAttributedString: NSMutableAttributedString!
    
    var descriptionAttributedTextModel: FeedAttributedTextModel!
    var commentsAttributedTextModel: FeedAttributedTextModel!

    var photos: Array<FeedPhotoModel> = Array()
    var photosRealCount: Int = 0
    
    var likes: Array<VKUser> = Array()
    var likesRealCount: Int = 0
    
    var comments: Array<FeedCommentModel> = Array()
    var commentsRealCount: Int = 0
    
    //help geometric
    var centerContainerHeight: CGFloat = 0
    var likesViewOffsetPoint = CGPointZero
    var rowHeight: CGFloat = 0
    
    //Dynamic vars
    var vkFirstPhoto: VKFeedPhoto {
        return self.vkFeedPost.photos.objectAtIndex(0) as VKFeedPhoto
    }
    
    var firstPhoto: FeedPhotoModel {
        return self.photos[0]
    }
    
    var canComment: Bool {
        return self.isSinglePhoto()
    }
    
    func isSinglePhoto() -> Bool {
        return self.photos.count == 1
    }
    
    init(vkFeedPost: VKFeedPost) {
        self.vkFeedPost = vkFeedPost
        
        self.user = UsersManager.get(vkFeedPost.source_id.integerValue)
        self.userFullname = self.user != nil ? self.user!.fullname : "Deleted"
       
        self.generateStatusAttributedString()
        
        self.generatePhotoModelsArray()
        self.generatePhotoDescription()
        self.generatePhotoLikes()
        self.generatePhotoComments()
        
        self.uniqueIndex = "\(vkFeedPost.type)_\(vkFeedPost.source_id.integerValue)_\(self.photosRealCount)_\(vkFeedPost.post_id.integerValue)"
        
        self.calculateRowHeight()
    }
    
    func generateStatusAttributedString() {

        let firstPhoto = self.vkFirstPhoto
        
        self.statusAttributedString = NSMutableAttributedString()
        self.statusAttributedString.beginEditing()
        
        if(self.isSinglePhoto()) {
            if(firstPhoto.locationLat != nil && firstPhoto.locationLong != nil) {
                let locationString = "\(firstPhoto.locationLat.floatValue) \(firstPhoto.locationLong.floatValue)"
                
                self.statusAttributedString.appendAttributedString(NSAttributedString(attachment: locationLabelTextAttach()))
                self.statusAttributedString.appendAttributedString(NSAttributedString(string: locationString, attributes: [NSKernAttributeName: 0.25]))
                self.statusAttributedString.appendAttributedString(NSAttributedString(string: " "))
                self.statusAttributedString.appendAttributedString(NSAttributedString(attachment: subtitleRoundDividerAttach()))
                self.statusAttributedString.appendAttributedString(NSAttributedString(string: " ", attributes: [NSKernAttributeName: 0.25]))
            }
            
        }
        
        self.statusAttributedString.appendAttributedString(NSAttributedString(string: relativeDateStringFromTimestamp(firstPhoto.date.doubleValue), attributes: [NSKernAttributeName: 0.25]))
        
        self.statusAttributedString.endEditing()
    }
    
    func generatePhotoComments() {
        if(!self.isSinglePhoto() || self.vkFirstPhoto.comments == nil) {
            return
        }
        
        let listArray = self.vkFirstPhoto.comments
        self.commentsRealCount = Int(listArray.count)
        
        for(var i = 0; i < listArray.items.count; i++) {
            let comment = listArray.items.objectAtIndex(i) as VKFeedPhotoComment
            self.comments.append(FeedCommentModel(feedComment: comment))
        }
        
        self.generatePhotoCommentsAttributedModel()
    }
    
    func generatePhotoCommentsAttributedModel() {
        let resultAttributedString = NSMutableAttributedString()
        
        if(self.commentsRealCount > 3) {
            let attributedString = NSAttributedString(string: TTTLocalizedPluralString(Int(self.commentsRealCount), "FeedComments") + "\n", attributes: [
                NSFontAttributeName: UIFont(name: "HelveticaNeue-Medium", size: 13)!,
                NSForegroundColorAttributeName: UIColorFromRGB(0xababab),
                NSKernAttributeName: 0.22
            ])
            resultAttributedString.appendAttributedString(NSAttributedString(string: "\n", attributes: [
                NSFontAttributeName: UIFont(name: "HelveticaNeue-Medium", size: 5)!
            ]))
            resultAttributedString.appendAttributedString(attributedString)
            resultAttributedString.appendAttributedString(NSAttributedString(string: "\n", attributes: [
                NSFontAttributeName: UIFont(name: "HelveticaNeue-Medium", size: 8)!
            ]))
        }
        
        for model in self.comments.reverse() {
            resultAttributedString.appendAttributedString(model.attributedStringForFeed())
            resultAttributedString.appendAttributedString(commentSeparatorAttributedString())
        }
        
        resultAttributedString.detectLinksAndAddLinkAttributes()
        resultAttributedString.detectTagsAndAddLinkAttributes()
        resultAttributedString.detectVKLinksAndAddAttributes()
        
        self.commentsAttributedTextModel = resultAttributedString.createAttributedStringModel(defaultCommentsLabel())
        self.commentsAttributedTextModel.calculateSizeForWidth(PHOTO_WIDTH - 28)
    }

    
    func generatePhotoLikes() {
        if(!self.isSinglePhoto() || self.vkFirstPhoto.likes == nil) {
            return
        }
        
        let likesArray = self.vkFirstPhoto.likes
        self.likesRealCount = Int(likesArray.count)
        
        for(var i = 0; i < likesArray.items.count; i++) {
            let likeUserId = likesArray.objectAtIndex(i) as NSNumber
            let user = UsersManager.get(likeUserId.integerValue);
            if(user != nil) {
                self.likes.append(user!)
            }
        }
    }
    
    func generatePhotoDescription() {
        if(!isSinglePhoto()) {
            return
        }
        
        let text = self.vkFirstPhoto.text as NSString!
        if(text == nil || text == "") {
            return
        }
        
        let defaultLabel = defaultDescriptionLabel()
        defaultLabel.setText(text)
        
        let resultAttributedString = defaultLabel.attributedText.mutableCopy() as NSMutableAttributedString
        resultAttributedString.addAttribute(NSKernAttributeName, value: 0.15, range: NSMakeRange(0, resultAttributedString.length))
        
        resultAttributedString.detectLinksAndAddLinkAttributes()
        resultAttributedString.detectTagsAndAddLinkAttributes()
        resultAttributedString.detectVKLinksAndAddAttributes()
        
        self.descriptionAttributedTextModel = resultAttributedString.createAttributedStringModel(defaultDescriptionLabel())
        self.descriptionAttributedTextModel.calculateSizeForWidth(PHOTO_WIDTH - 28)
    }
    
    func generatePhotoModelsArray() {
        
        let vkFeedPhotos = self.vkFeedPost.photos
        self.photosRealCount = Int(vkFeedPhotos.count)

        if(self.photosRealCount <= 6 || vkFeedPhotos.items.count < 6)  {
            self.photosRealCount = vkFeedPhotos.items.count
        }
        
        var rectList: Array<CGRect> = Array()
        
        switch(vkFeedPhotos.items.count) {
        case 1:
            rectList.append(CGRectMake(0, 0, PHOTO_WIDTH, PHOTO_HEIGHT))
        case 2:
            rectList.append(CGRectMake(0, 0, PHOTO_WIDTH_2 - 1, PHOTO_HEIGHT))
            rectList.append(CGRectMake(PHOTO_WIDTH_2 + 1, 0, PHOTO_WIDTH_2 - 1, PHOTO_HEIGHT))
        case 3:
            rectList.append(CGRectMake(0, 0, PHOTO_WIDTH - PHOTO_SCALE_WIDTH_2 - 1, PHOTO_HEIGHT))
            rectList.append(CGRectMake(PHOTO_WIDTH - PHOTO_SCALE_WIDTH_2 + 1, 0, PHOTO_SCALE_WIDTH_2 - 1, PHOTO_HEIGHT_2 - 1))
            rectList.append(CGRectMake(PHOTO_WIDTH - PHOTO_SCALE_WIDTH_2 + 1, PHOTO_HEIGHT_2 + 1, PHOTO_SCALE_WIDTH_2 - 1,  PHOTO_HEIGHT - PHOTO_HEIGHT_2 - 1))
        case 4:
            rectList.append(CGRectMake(0, 0, PHOTO_WIDTH_2 - 1, PHOTO_HEIGHT_2 - 1))
            rectList.append(CGRectMake(PHOTO_WIDTH_2 + 1, 0, PHOTO_WIDTH_2 - 1, PHOTO_HEIGHT_2 - 1))
            rectList.append(CGRectMake(0, PHOTO_HEIGHT_2 + 1, PHOTO_WIDTH_2 - 1, PHOTO_HEIGHT - PHOTO_HEIGHT_2 - 1))
            rectList.append(CGRectMake(PHOTO_WIDTH_2 + 1, PHOTO_HEIGHT_2 + 1, PHOTO_WIDTH_2 - 1, PHOTO_HEIGHT - PHOTO_HEIGHT_2 - 1))
        case 5:
            rectList.append(CGRectMake(0, 0, PHOTO_2WIDTH_3, PHOTO_HEIGHT - PHOTO_SCALE_HEIGHT_3 - 1))
            rectList.append(CGRectMake(PHOTO_2WIDTH_3 + 2, 0, PHOTO_WIDTH_3 - 2, PHOTO_HEIGHT - PHOTO_SCALE_HEIGHT_3 - 1))
            rectList.append(CGRectMake(0, PHOTO_HEIGHT - PHOTO_SCALE_HEIGHT_3 + 1, PHOTO_WIDTH_3 - 2, PHOTO_SCALE_HEIGHT_3 - 1))
            rectList.append(CGRectMake(PHOTO_WIDTH_3, PHOTO_HEIGHT - PHOTO_SCALE_HEIGHT_3 + 1, PHOTO_2WIDTH_3 - PHOTO_WIDTH_3, PHOTO_SCALE_HEIGHT_3 - 1))
            rectList.append(CGRectMake(PHOTO_2WIDTH_3 + 2, PHOTO_HEIGHT - PHOTO_SCALE_HEIGHT_3 + 1, PHOTO_WIDTH_3 - 2, PHOTO_SCALE_HEIGHT_3 - 1))
        default:
            rectList.append(CGRectMake(0, 0, PHOTO_2WIDTH_3, PHOTO_2HEIGHT_3))
            rectList.append(CGRectMake(PHOTO_2WIDTH_3 + 2, 0, PHOTO_WIDTH_3 - 2, PHOTO_HEIGHT_3 - 1))
            rectList.append(CGRectMake(PHOTO_2WIDTH_3 + 2, PHOTO_HEIGHT_3 + 1, PHOTO_WIDTH_3 - 2, PHOTO_2HEIGHT_3 - PHOTO_HEIGHT_3 - 1))
            rectList.append(CGRectMake(0, PHOTO_2HEIGHT_3 + 2, PHOTO_WIDTH_3 - 2, PHOTO_HEIGHT_3 - 2))
            rectList.append(CGRectMake(PHOTO_WIDTH_3, PHOTO_2HEIGHT_3 + 2, PHOTO_2WIDTH_3 - PHOTO_WIDTH_3, PHOTO_HEIGHT_3 - 2))
            rectList.append(CGRectMake(PHOTO_2WIDTH_3 + 2, PHOTO_2HEIGHT_3 + 2, PHOTO_WIDTH_3 - 2, PHOTO_HEIGHT_3 - 2))
        }
        
        for (index, rect) in enumerate(rectList) {
            let feedPhotoModel = FeedPhotoModel(rect: rect, feedPostModel: self, feedPhoto: vkFeedPhotos.objectAtIndex(index) as VKFeedPhoto)
            self.photos.append(feedPhotoModel)
        }
        
    }
    
    func calculateRowHeight() {
        self.rowHeight = PHOTO_HEIGHT + 63
        
        if(self.descriptionAttributedTextModel != nil) {
            self.rowHeight += self.descriptionAttributedTextModel.size.height + 5
            
            self.centerContainerHeight += self.descriptionAttributedTextModel.size.height + 5
            
            if(self.likes.count == 0) {
                self.rowHeight += 16
                self.centerContainerHeight += 16
            } else {
                self.likesViewOffsetPoint = CGPointMake(14, 20 + self.descriptionAttributedTextModel.size.height)
            }
        } else {
            self.likesViewOffsetPoint = CGPointMake(14, 15)
        }
        
        if(self.likes.count != 0) {
            self.rowHeight += 66
            self.centerContainerHeight += 66
        }
        
        if(self.comments.count != 0) {
            self.rowHeight += self.commentsAttributedTextModel.size.height - 8 + (self.commentsRealCount > 3 ? 1 : OnePixelInset)
        }
    }
}


func CreateFeedPhotoDescriptionLabel() -> FeedTTTAttributedLabel {
    let descriptionLabel = FeedTTTAttributedLabel()
    descriptionLabel.numberOfLines = 0
    descriptionLabel.font = UIFont(name: "HelveticaNeue", size: 15)
    descriptionLabel.textColor = UIColorFromRGB(0x18191a)
    descriptionLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
    descriptionLabel.minimumLineHeight = 19
    descriptionLabel.maximumLineHeight = 19
    descriptionLabel.linkAttributes = [kCTForegroundColorAttributeName: UIColorFromRGB(0x49719e)]
    descriptionLabel.activeLinkAttributes = [kCTForegroundColorAttributeName: UIColorFromRGB(0x193a60)]
    descriptionLabel.inactiveLinkAttributes = [kCTForegroundColorAttributeName: UIColorFromRGB(0x193a60)]
    return descriptionLabel
}

func CreateFeedPhotoCommentsLabel() -> FeedTTTAttributedLabel {
    let descriptionLabel = FeedTTTAttributedLabel()
    descriptionLabel.numberOfLines = 0
    descriptionLabel.font = UIFont(name: "HelveticaNeue", size: 15)
    descriptionLabel.textColor = UIColorFromRGB(0x666666)
    descriptionLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
    descriptionLabel.minimumLineHeight = 19
    descriptionLabel.maximumLineHeight = 19
    descriptionLabel.linkAttributes = [kCTForegroundColorAttributeName: UIColorFromRGB(0x4774a8)]
    descriptionLabel.activeLinkAttributes = [kCTForegroundColorAttributeName: UIColorFromRGB(0x183c65)]
    descriptionLabel.inactiveLinkAttributes = [kCTForegroundColorAttributeName: UIColorFromRGB(0x183c65)]
    return descriptionLabel
}


let PHOTO_WIDTH = UIScreen.mainScreen().bounds.width
let PHOTO_HEIGHT = normalizePixexForDeviceScale(PHOTO_WIDTH * 8.0 / 9.0)

let PHOTO_WIDTH_2 = PHOTO_WIDTH / 2.0
let PHOTO_HEIGHT_2 = normalizePixexForDeviceScale(PHOTO_HEIGHT / 2.0)

let PHOTO_SCALE_WIDTH_2 = normalizePixexForDeviceScale(PHOTO_HEIGHT_2 * 9.0 / 8.0)

let PHOTO_WIDTH_3 = floor(PHOTO_WIDTH / 3.0)
let PHOTO_HEIGHT_3 = normalizePixexForDeviceScale(PHOTO_HEIGHT / 3.0)

let PHOTO_SCALE_HEIGHT_3 = normalizePixexForDeviceScale(PHOTO_WIDTH_3 * 8.0 / 9.0)

let PHOTO_2WIDTH_3 = PHOTO_WIDTH - PHOTO_WIDTH_3
let PHOTO_2HEIGHT_3 = PHOTO_HEIGHT - PHOTO_HEIGHT_3

var _defaultCommentsLabel: TTTAttributedLabel!
func defaultCommentsLabel() -> TTTAttributedLabel {
    if(_defaultCommentsLabel == nil) {
        _defaultCommentsLabel = CreateFeedPhotoCommentsLabel()
    }
    return _defaultCommentsLabel
}

var _defaultDescriptionLabel: TTTAttributedLabel!
func defaultDescriptionLabel() -> TTTAttributedLabel {
    if(_defaultDescriptionLabel == nil) {
        _defaultDescriptionLabel = CreateFeedPhotoDescriptionLabel()
    }
    return _defaultDescriptionLabel
}

var _locationLabelTextAttach: NSTextAttachment!
func locationLabelTextAttach() -> NSTextAttachment {
    if(_locationLabelTextAttach == nil) {
        let imageGeo = extendImage(UIImage(named: "LocationLabelIcon")!, UIEdgeInsetsMake(0, 1, 0, 5 + OnePixelInset))
        
        _locationLabelTextAttach = NSTextAttachment()
        _locationLabelTextAttach.image = imageGeo
        _locationLabelTextAttach.bounds = CGRectMake(0, -2, imageGeo.size.width, imageGeo.size.height)
    }
    return _locationLabelTextAttach
}

var _subtitleRoundDividerAttach: NSTextAttachment!
func subtitleRoundDividerAttach() -> NSTextAttachment {
    if(_subtitleRoundDividerAttach == nil) {
        let image = extendImage(UIImage(named: "SubtitleRoundDivider")!, UIEdgeInsetsMake(0, 1 + OnePixelInset, 0, 2))
        
        _subtitleRoundDividerAttach = NSTextAttachment()
        _subtitleRoundDividerAttach.image = image
        _subtitleRoundDividerAttach.bounds = CGRectMake(0, 2, image.size.width, image.size.height)
    }
    return _subtitleRoundDividerAttach
}

var _commentSeparatorAttributedString: NSAttributedString!
func commentSeparatorAttributedString() -> NSAttributedString {
    if(_commentSeparatorAttributedString == nil) {
        let mutableParagraph = NSMutableParagraphStyle()
        mutableParagraph.maximumLineHeight = 9
        mutableParagraph.minimumLineHeight = 9
        
        _commentSeparatorAttributedString = NSAttributedString(string: "\n \n", attributes: [
            NSParagraphStyleAttributeName: mutableParagraph
        ])
    }
    return _commentSeparatorAttributedString
}