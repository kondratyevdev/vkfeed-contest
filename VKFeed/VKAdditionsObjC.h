//
//  VKAdditionsObjC.h
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/13/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VKSdk.h"

@interface VKFeedPhotoComment : VKApiObject
@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) NSNumber *from_id;
@property (nonatomic, strong) NSNumber *date;
@property (nonatomic, strong) NSString *text;
@end

@interface VKFeedPhotoCommentArray : VKApiObjectArray
@end

@interface VKFeedPhotoLikeArray : VKApiObjectArray
@end

@interface VKFeedPhoto : VKPhoto
@property (nonatomic, strong) VKFeedPhotoCommentArray *comments;
@property (nonatomic, strong) VKFeedPhotoLikeArray *likes;
@property (nonatomic, strong) NSNumber *locationLat;
@property (nonatomic, strong) NSNumber *locationLong;
@end

@interface VKFeedPhotoArray : VKApiObjectArray
@end

@interface VKFeedPost : VKApiObject
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSNumber *source_id;
@property (nonatomic, strong) NSNumber *date;
@property (nonatomic, strong) NSNumber *post_id;
@property (nonatomic, strong) VKFeedPhotoArray *photos;
@end

@interface VKFeedPostArray : VKApiObjectArray
@property (nonatomic, strong) VKUsersArray *users;
@property (nonatomic, strong) VKGroups *groups;
@property (nonatomic, strong) NSString *next_from;
@end

@interface VKApiNewsFeed : VKApiBase
- (VKRequest *)getPhotoFeed:(NSDictionary *)params;
@end

@interface VKApi (NewsFeedAPI)
+ (VKApiNewsFeed *)newsFeed;
@end