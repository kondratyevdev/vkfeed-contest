//
//  FeedHeaderCellView.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/13/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

let MoreActionImage = extendImage(UIImage(named: "MoreActions")!, UIEdgeInsetsMake(0, 5, 0, 5))

class FeedHeaderCellView: UIView, UIActionSheetDelegate {
    
    let userAvatarImageView: UserAvatarImageView!
    let userFullNameLabel: UILabel!
    let statusLabel: UILabel!
    let moreActionsButton: UIButton!
    
    var feedPost: FeedPostModel! {
        didSet {
            if(self.feedPost.uniqueIndex != oldValue?.uniqueIndex) {
                feedPostModelDidChanged()
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor(white: 1.0, alpha: 0.9)
        
        self.userAvatarImageView = UserAvatarImageView(frame: CGRectMake(10, 10, 45, 45))
        self.userAvatarImageView.syncWithDisk = false
        self.addSubview(self.userAvatarImageView)
        
        self.moreActionsButton = UIButton()
        self.moreActionsButton.setBackgroundImage(MoreActionImage, forState: UIControlState.Normal)
        self.moreActionsButton.sizeToFit()
        self.moreActionsButton.addTarget(self, action: "moreActionsButtonDidTouchUp", forControlEvents: UIControlEvents.TouchUpInside)
        self.moreActionsButton.frame.origin = CGPointMake(self.frame.size.width - 15 - self.moreActionsButton.frame.size.width, 24 + OnePixelInset)
        self.moreActionsButton.autoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleRightMargin
        self.addSubview(self.moreActionsButton)
        
        self.userFullNameLabel = UILabel()
        self.userFullNameLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        self.userFullNameLabel.textColor = UIColorFromRGB(0x4774a8)
        self.userFullNameLabel.lineBreakMode = NSLineBreakMode.ByTruncatingTail
        self.userFullNameLabel.frame = CGRectNormalizeForScale(CGRectMake(62, 14, self.moreActionsButton.frame.origin.x - 62 - 10, self.userFullNameLabel.font.lineHeight))
        self.addSubview(self.userFullNameLabel)
        
        self.statusLabel = UILabel()
        self.statusLabel.font = UIFont(name: "HelveticaNeue", size: 13)
        self.statusLabel.textColor = UIColorFromRGB(0x939699)
        self.statusLabel.lineBreakMode = NSLineBreakMode.ByTruncatingTail
        self.statusLabel.frame = CGRectNormalizeForScale(CGRectMake(62, 35, self.userFullNameLabel.frame.size.width, self.statusLabel.font.lineHeight))
        self.addSubview(self.statusLabel)
        
    }
    
    func feedPostModelDidChanged() {
        self.userAvatarImageView.user = self.feedPost.user
        self.userFullNameLabel.text = self.feedPost.userFullname
        self.statusLabel.attributedText = self.feedPost.statusAttributedString
    }
    
    func moreActionsButtonDidTouchUp() {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil)
        
        if(self.feedPost.isSinglePhoto()) {
            actionSheet.addButtonWithTitle("Open in Safari")
            
            if(self.feedPost.canComment) {
                actionSheet.addButtonWithTitle("Comment")
            }
        }
        
        actionSheet.addButtonWithTitle("Show User Profile")
        actionSheet.showFromRect(self.moreActionsButton.frame, inView: self.moreActionsButton, animated: true)
    }
    
    func actionSheet(actionSheet: UIActionSheet, didDismissWithButtonIndex buttonIndex: Int) {
        let buttonTitle = actionSheet.buttonTitleAtIndex(buttonIndex)
        
        if(buttonTitle == "Open in Safari") {
//            UIApplication.sharedApplication().openURL(self.feedPost.webURL)
        }
    }
}