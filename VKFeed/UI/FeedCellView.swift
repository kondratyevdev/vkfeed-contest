//
//  FeedCellView.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/13/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

class FeedTTTAttributedLabel : TTTAttributedLabel {
    
    var textChecked = false
    
    func setAttributedTextModel(model: FeedAttributedTextModel) {
        self.textChecked = false
        self.attributedText = model.attributedString
        self.frame.size = model.size
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        if(!self.textChecked) {
            self.enabledTextCheckingTypes = NSTextCheckingType.Link.rawValue
            self.setText(self.attributedText)
            self.enabledTextCheckingTypes = NSTextCheckingType.allZeros.rawValue
            self.textChecked = true
        }
        
        super.touchesBegan(touches, withEvent: event)
    }
}

class FeedCellBottomView : UIView {
    override func drawRect(rect: CGRect) {
        UIColorFromRGB(0xf2f3f5).setFill()
        UIBezierPath(rect: CGRectMake(0, self.frame.size.height - 1, self.frame.size.width, 1)).fill()
    }
}

class FeedCellView : UITableViewCell, TTTAttributedLabelDelegate, UIAlertViewDelegate {

    var photoViews: Array<FeedPhotoImageView> = Array()
    
    var centerContainerView: UIView!
    
    var descriptionLabel: FeedTTTAttributedLabel!
    var likesView: FeedLikesView!
    
    var commentsLabel: FeedTTTAttributedLabel!
    var commentButton: FeedStandartGrayButton!
    var likeButton: FeedLikeGrayButton!
    
    var feedPost: FeedPostModel! {
        didSet {
            self.feedPostModelDidChanged()
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "cell")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.whiteColor()
        
        for(var i = 0; i < 6; i++) {
            let feedPhotoImageView = FeedPhotoImageView()
            self.photoViews.append(feedPhotoImageView)
            self.addSubview(feedPhotoImageView)
        }
        
        self.centerContainerView = UIView(frame: CGRectMake(0, PHOTO_HEIGHT, self.frame.size.width, 0))
        self.centerContainerView.autoresizingMask = UIViewAutoresizing.FlexibleWidth
        self.centerContainerView.backgroundColor = UIColorFromRGB(0xf5f7fa)
        self.addSubview(self.centerContainerView)
        
        self.descriptionLabel = CreateFeedPhotoDescriptionLabel()
        self.descriptionLabel.enabledTextCheckingTypes = NSTextCheckingType.allZeros.rawValue
        self.descriptionLabel.frame = CGRectMake(13 + OnePixelInset, 9, 0, 0)
        self.descriptionLabel.delegate = self
        self.descriptionLabel.backgroundColor = UIColorFromRGB(0xf5f7fa)
        self.centerContainerView.addSubview(self.descriptionLabel)
        
        self.likesView = FeedLikesView(frame: CGRectMake(0, 0, self.frame.size.width - 28, 39))
        self.likesView.autoresizingMask = UIViewAutoresizing.FlexibleWidth
        self.centerContainerView.addSubview(self.likesView)
        
        self.commentsLabel = CreateFeedPhotoDescriptionLabel()
        self.commentsLabel.delegate = self
        self.commentsLabel.enabledTextCheckingTypes = NSTextCheckingType.allZeros.rawValue
        self.commentsLabel.frame = CGRectMake(13 + OnePixelInset, 9, 0, 0)
        self.commentsLabel.backgroundColor = UIColor.whiteColor()
        self.addSubview(self.commentsLabel)
        
        let bottomView = FeedCellBottomView(frame: CGRectMake(0, self.frame.size.height - 63, self.frame.size.width, 63))
        bottomView.backgroundColor = UIColor.whiteColor()
        bottomView.autoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleWidth
        self.addSubview(bottomView)
        
        self.commentButton = FeedStandartGrayButton(frame: CGRectMake(0, 14, 0, 34))
        self.commentButton.setTitle(NALocalizedString("feedAction.comment"), forState: UIControlState.Normal)
        self.commentButton.addTarget(self, action: "commentButtonAction", forControlEvents: UIControlEvents.TouchUpInside)
        self.commentButton.sizeToFit()
        bottomView.addSubview(self.commentButton)
        
        self.likeButton = FeedLikeGrayButton(frame: CGRectMake(14, 14, 0, 34))
        bottomView.addSubview(self.likeButton)
    }
    
    override func setHighlighted(highlighted: Bool, animated: Bool) {}
    
    override func setSelected(selected: Bool, animated: Bool) {}
    
    func feedPostModelDidChanged() {

        let photosCount = self.feedPost.photos.count
        for (index, feedPhotoImageView) in enumerate(self.photoViews) {
            if(photosCount > index) {
                feedPhotoImageView.hidden = false
                feedPhotoImageView.photo = self.feedPost.photos[index]
                
                if(index == 5) {
                    if(self.feedPost.photosRealCount > 6) {
                        feedPhotoImageView.showPhotosCount(self.feedPost.photosRealCount)
                    } else {
                        feedPhotoImageView.hidePhotosCount()
                    }
                }
                
            } else {
                feedPhotoImageView.hidden = true
                feedPhotoImageView.cancel()
            }
        }
        
        if(self.feedPost.centerContainerHeight != 0) {
            self.centerContainerView.hidden = false
            self.centerContainerView.frame.size = CGSizeMake(self.centerContainerView.frame.size.width, self.feedPost.centerContainerHeight)
            
            if(self.feedPost.descriptionAttributedTextModel != nil) {
                self.descriptionLabel.hidden = false
            } else {
                self.descriptionLabel.hidden = true

            }
            
            if(self.feedPost.descriptionAttributedTextModel != nil) {
                self.descriptionLabel.hidden = false
                self.descriptionLabel.setAttributedTextModel(self.feedPost.descriptionAttributedTextModel)
            } else {
                self.descriptionLabel.hidden = true
            }
            
            if(self.feedPost.likes.count != 0) {
                self.likesView.frame.origin = self.feedPost.likesViewOffsetPoint
                self.likesView.likes = self.feedPost.likes
                self.likesView.hidden = false
            } else {
                self.likesView.cancelAll()
                self.likesView.hidden = true
            }
            
        } else {
            self.centerContainerView.hidden = true
        }
        
        
        if(self.feedPost.comments.count != 0) {
            self.commentsLabel.hidden = false
            self.commentsLabel.setAttributedTextModel(self.feedPost.commentsAttributedTextModel)            
            self.commentsLabel.frame.origin = CGPointMake(self.commentsLabel.frame.origin.x, PHOTO_HEIGHT + self.feedPost.centerContainerHeight + 5 + (self.feedPost.commentsRealCount > 3 ? 1 : OnePixelInset))
        } else {
            self.commentsLabel.hidden = true
        }
        
        self.likeButton.likesCount = self.feedPost.likesRealCount
        self.commentButton.frame.origin = CGPointMake(self.likeButton.frame.origin.x + self.likeButton.frame.size.width + 11, self.commentButton.frame.origin.y)
    }
    
    func attributedLabel(label: TTTAttributedLabel!, didSelectLinkWithURL url: NSURL!) {
        if(url == nil) {
            return
        }
        
        let absoluteString = url.absoluteString! as NSString
        if(absoluteString.hasPrefix("tag://")) {
            let tag = absoluteString.substringFromIndex(6).stringByReplacingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
            showAlertView("Tag", tag)
            return
        }
        
        if(absoluteString.hasPrefix("vklink://")) {
            showAlertView("VKUser", "user_id: " + absoluteString.substringFromIndex(9))
            return
        }
        
        UIApplication.sharedApplication().openURL(url)
    }
    
    func commentButtonAction() {
//        if(self.feedPost.canComment) {
//            showAlertView("Error", "This function will be added soon")
//
////            let alertView = UIAlertView(title: "Send a comment", message: "", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Send")
////            alertView.alertViewStyle = UIAlertViewStyle.PlainTextInput
////            alertView.show()
//        } else {
//            showAlertView("Error", "Can't add comment to this post ;(")
//        }
    }
    
    func alertView(alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if(alertView.alertViewStyle == UIAlertViewStyle.PlainTextInput) {
            if(alertView.buttonTitleAtIndex(buttonIndex) == "Send") {
                let text = alertView.textFieldAtIndex(0)?.text
                if(countElements(text!) < 5) {
                    showAlertView("Error", "Comment must be longer than 5 chars ;(")
                    return
                }
        
            }
        }
    }
}
