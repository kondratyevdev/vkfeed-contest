//
//  NAQueue.swift
//  NAKit
//
//  Created by Dmitry Kondratyev on 6/30/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//
//  Thanks:
//  Peter Iakovlev

import UIKit

var NAQueueMain = NAQueue(queue: dispatch_get_main_queue())
var NAQueueStage: NAQueue = NAQueue(name: "com.test.Stage")

class NAQueue {
    
    var queue: dispatch_queue_t?
    var name: UnsafePointer<Int8>
    
    init(name: String) {
        self.name = NSString(string: name).UTF8String
        self.queue = dispatch_queue_create(self.name, nil)
        
   
        dispatch_queue_set_specific(self.queue, self.name, &self.name, nil)
        
        self.dispatchOnQueue({
            NSThread.currentThread().name = name
        })
    }
    
    init(queue: dispatch_queue_t!) {
        self.name = dispatch_queue_get_label(self.queue)
        self.queue = queue
    }
    
    func dispatchOnQueue(block: dispatch_block_t!) {
        self.dispatchOnQueue(block: block, synchroinize: false)
    }
    
    func dispatchOnQueue(#block: dispatch_block_t!, synchroinize: Bool) {
        if ((block == nil) || (self.queue == nil)) {
            return
        }
        
        if(dispatch_get_specific(&self.name) == &self.name) {
            block()
        } else if(synchroinize) {
            dispatch_sync(self.queue, block)
        } else {
            dispatch_async(self.queue, block)
        }
    }
    
    class func dispatchOnMainQueue(block: dispatch_block_t!) {
        NAQueueMain.dispatchOnQueue(block)
    }
    
    class func dispatchOnStageQueue(block: dispatch_block_t!) {
        NAQueueStage.dispatchOnQueue(block)
    }
}