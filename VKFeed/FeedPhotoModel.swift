//
//  FeedPhotoModel.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/18/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

class FeedPhotoModel {
    
    let frame: CGRect
    let src: String
    
    init(rect: CGRect, feedPostModel: FeedPostModel, feedPhoto: VKFeedPhoto) {
        self.frame = rect
        
        let necessaryPhotoWidth = Float(rect.width * UIScreen.mainScreen().scale)
        let neccessryPhotoHeight = Float(rect.height * UIScreen.mainScreen().scale)
        
        let photoSizes = feedPhoto.sizes
        var photoSize = (photoSizes.objectAtIndex(0) as VKPhotoSize!)
        
        for(var i = 0; i < photoSizes.items.count; i++) {
            let size = photoSizes.objectAtIndex(i) as VKPhotoSize
            
            if(size.width.integerValue > photoSize.width.integerValue) {
                if(photoSize.width.floatValue < necessaryPhotoWidth || photoSize.height.floatValue < neccessryPhotoHeight) {
                    photoSize = size
                }
            }
            
        }
        
        self.src = photoSize.src
    }
}