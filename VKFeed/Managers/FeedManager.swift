//
//  FeedManager.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/13/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

protocol FeedPostsManagerProtocol {
    
    func feedPostsManager(manager: FeedPostsManager?, addItemsInRange: NSRange);
    func feedPostsManager(manager: FeedPostsManager?, error: NSError);
    
}

class FeedPostsManager {
    
    init() {
        
    }
    
    var delegate: FeedPostsManagerProtocol?
    private var list: Array<FeedPostModel> = Array()
    private var nextFrom: String = ""
    
    var count: Int { return self.list.count }
    
    func objectAtIndex(index: Int) -> FeedPostModel {
        return self.list[index]
    }
    
    func reset() {
        self.list.removeAll(keepCapacity: false)
    }
    
    var isRefreshFeedNow = false
    func refreshFeed() {
        if(self.isRefreshFeedNow) {
            return
        }
        
        self.isRefreshFeedNow = true
        self.loadItems("")
    }
    
    var isLoadingMoreNow = false
    func loadMoreItems() {
        if(self.isRefreshFeedNow || self.isLoadingMoreNow) {
            return
        }
        
        self.isLoadingMoreNow = true
        self.loadItems(self.nextFrom)
    }
    
    func loadItems(nextFrom: String) {
        NSLog("Loading posts \(nextFrom)")
        VKApi.newsFeed().getPhotoFeed([
            "start_from": nextFrom,
            "count": 10,
            "likes_count": FeedLikesCount
        ]).executeWithResultBlock({ (response) -> Void in
            let items = response.parsedModel as VKFeedPostArray
            
            NSLog("Success load posts \(nextFrom)")
            
            NAQueueStage.dispatchOnQueue({ () -> Void in
                self.itemsLoaded(start_from: nextFrom, items: items)
            })
            
        }, errorBlock: { (error) -> Void in
            NSLog("VK error: %@", error)
            self.delegate?.feedPostsManager(self, error: error)
        })
    }
    
    func itemsLoaded(#start_from: String, items: VKFeedPostArray) {
        UsersManager.addUsers(items.users.items)
        
        var feedItems: Array<FeedPostModel> = Array()
        
        for(var i = 0; i < items.items.count; i++) {
            let item = items.items[i] as VKFeedPost
            
            let listItem = FeedPostModel(vkFeedPost: item)
            feedItems.append(listItem)
        }

        NAQueueMain.dispatchOnQueue { () -> Void in

            if(start_from == "") {
                self.isRefreshFeedNow = false
                self.list.removeAll(keepCapacity: true)
            } else {
                self.isLoadingMoreNow = false
                if(self.nextFrom != start_from) {
                    return
                }
            }
            
            let startLocation = self.list.count
            for item in feedItems {
                self.list.append(item)
            }
            
            self.nextFrom = items.next_from
            
            self.delegate?.feedPostsManager(self, addItemsInRange: NSMakeRange(startLocation, feedItems.count))
        }
    }
}
