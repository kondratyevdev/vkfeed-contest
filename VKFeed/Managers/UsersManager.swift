//
//  UsersManager.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/13/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

private let _SingletonUsersManager = UsersManager()

class UsersManager {
    
    class var sharedInstance : UsersManager {
        return _SingletonUsersManager
    }
    
    class func get(id: Int) -> VKUser? {
        return self.sharedInstance.get(id)
    }
    
    class func addUsers(users: NSArray!) {
        self.sharedInstance.addUsers(users)
    }
    
    private var users: Dictionary<Int, VKUser> = Dictionary()
    
    init() {
        
    }
    
    func get(id: Int) -> VKUser? {
        return self.users[id]
    }
    
    func addUsers(users: NSArray!) {
        for(var i = 0; i < users.count; i++) {
            let user : VKUser? = users.objectAtIndex(i) as? VKUser
            if(user != nil) {
                self.users.updateValue(user!, forKey: user!.id.integerValue)
            }
        }
    }
}
