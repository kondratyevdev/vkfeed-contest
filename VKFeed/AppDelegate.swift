//
//  AppDelegate.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/12/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, VKSdkDelegate, UINavigationControllerDelegate {

    var window: UIWindow!
    var navigationController: UINavigationController!
    var feedViewController: FeedViewController!

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        self.feedViewController = FeedViewController()

        self.navigationController = UINavigationController(rootViewController: self.feedViewController)
        self.navigationController.navigationBarHidden = true
        self.navigationController.delegate = self
        
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window.rootViewController = self.navigationController
        self.window.makeKeyAndVisible()

        let statusBarBackgroundView = UIView(frame: CGRectMake(0, 0, self.window.frame.size.width, 20))
        statusBarBackgroundView.backgroundColor = UIColorFromRGB(0x4c75a3)
        self.window.rootViewController?.view.addSubview(statusBarBackgroundView)

        self.initializeVKSDK();
        return true
    }
    
    func initializeVKSDK() {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            VKSdk.initializeWithDelegate(self, andAppId: "4597531")
            if(!VKSdk.wakeUpSession()) {
                VKSdk.authorize(["wall", "friends", "photos"]);
            } else {
                self.feedViewController.manager.refreshFeed()
            }
        })
    }
    
    func navigationController(navigationController: UINavigationController, didShowViewController viewController: UIViewController, animated: Bool) {
        
    }
    
    func vkSdkNeedCaptchaEnter(captchaError: VKError!) {
        
    }
    
    func vkSdkTokenHasExpired(expiredToken: VKAccessToken!) {
        VKSdk.authorize(["wall", "friends", "photos"]);
    }
    
    func vkSdkUserDeniedAccess(authorizationError: VKError!) {
        
    }
    
    func vkSdkShouldPresentViewController(controller: UIViewController!) {
        self.navigationController.presentViewController(controller, animated: true, completion: nil)
    }
    
    func vkSdkReceivedNewToken(newToken: VKAccessToken!) {
        self.feedViewController.manager.refreshFeed()
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        VKSdk.processOpenURL(url, fromApplication: sourceApplication)
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

