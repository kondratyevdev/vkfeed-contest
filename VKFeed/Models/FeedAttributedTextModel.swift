//
//  AttributedStingModel.swift
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/21/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

import UIKit

extension NSAttributedString {
    func createAttributedStringModel(label: TTTAttributedLabel) -> FeedAttributedTextModel {
        let model = FeedAttributedTextModel()
        label.setText(self)
        model.attributedString = label.attributedText
        label.textRectForBounds(CGRectMake(0, 0, 100, 100), limitedToNumberOfLines: 0)
        return model
    }
}

class FeedAttributedTextModel {
   
    var attributedString: NSAttributedString!
    var size = CGSizeZero
    
    func calculateSizeForWidth(width: CGFloat) {
        
        let boungindRect = self.attributedString.boundingRectWithSize(CGSizeMake(width, 0), options: NSStringDrawingOptions.UsesLineFragmentOrigin, context: nil)
        
        self.size = CGSizeMake(width, ceil(boungindRect.size.height) + 2)
    }
    
}


