//
//  VKAdditionsObjC.m
//  VKFeed
//
//  Created by Dmitry Kondratyev on 11/13/14.
//  Copyright (c) 2014 kondra. All rights reserved.
//

#import "VKAdditionsObjC.h"

@implementation VKFeedPhoto
- (instancetype) initWithDictionary:(NSDictionary *)dict {
    self = [super initWithDictionary:dict];
    self.locationLat = [dict objectForKey:@"lat"];
    self.locationLong = [dict objectForKey:@"long"];
    return self;
}
@end

@implementation VKFeedPhotoArray
- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super initWithDictionary:dict objectClass:[VKFeedPhoto class]];
    
    return self;
}
@end

@implementation VKFeedPhotoComment
@end

@implementation VKFeedPhotoLikeArray

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super initWithDictionary:dict objectClass:[NSNumber class]];
    return self;
}

@end

@implementation VKFeedPhotoCommentArray

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    // fix bug with empty array class
    return [super initWithDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[NSMutableArray arrayWithArray:[dict objectForKey:@"items"]], @"items", [dict objectForKey:@"count"], @"count", nil] objectClass:[VKFeedPhotoComment class]];
}

@end

@implementation VKFeedPost
@end

@implementation VKFeedPostArray

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super initWithDictionary:dict objectClass:[VKFeedPost class]];
    
    NSDictionary *response = dict[@"response"];
    if(response != nil) {
        NSArray *profiles = response[@"profiles"];
        if(profiles != nil) {
            self.users = [[VKUsersArray alloc] initWithArray:profiles objectClass:[VKUser class]];
        }
        
        NSArray *groups = response[@"groups"];
        if(groups != nil) {
            self.groups = [[VKGroups alloc] initWithArray:groups objectClass:[VKGroup class]];
        }
        
        self.next_from = response[@"next_from"];
    }
    return self;
}

@end

@implementation VKApiNewsFeed

- (VKRequest *)getPhotoFeed:(NSDictionary *)inputParams {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:inputParams];
    params[@"v"] = @"5.26";
    return [VKRequest requestWithMethod:@"newsfeed.getPhotoFeed" andParameters:params andHttpMethod:@"GET" classOfModel:[VKFeedPostArray class]];
}

@end

@implementation VKApi (NewsFeedAPI)

+ (VKApiNewsFeed *)newsFeed {
    static VKApiNewsFeed *api = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        api = [[VKApiNewsFeed alloc] init];
    });
    return api;
}

@end